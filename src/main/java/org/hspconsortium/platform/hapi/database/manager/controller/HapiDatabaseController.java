/**
 *  * #%L
 *  *
 *  * %%
 *  * Copyright (C) 2014-2020 Healthcare Services Platform Consortium
 *  * %%
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *  * #L%
 */

package org.hspconsortium.platform.hapi.database.manager.controller;

import org.hspconsortium.platform.hapi.database.manager.model.DataSet;
import org.hspconsortium.platform.hapi.database.manager.model.Sandbox;
import org.hspconsortium.platform.hapi.database.manager.service.HapiDatabaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("${hspc.platform.api.sandboxPath:/{teamId}/sandbox}")
public class HapiDatabaseController {

    private static final Logger logger = LoggerFactory.getLogger(HapiDatabaseController.class);

    private HapiDatabaseService hapiDatabaseService;

    @Autowired
    public HapiDatabaseController(HapiDatabaseService hapiDatabaseService) {
        this.hapiDatabaseService = hapiDatabaseService;
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Sandbox save(HttpServletRequest request, @PathVariable String teamId, @NotNull @RequestBody Sandbox sandbox,
                        @RequestParam(value = "dataSet", required = false) DataSet dataSet) {
//        Validate.notNull(sandbox);
//        Validate.notNull(sandbox.getTeamId());

//        don't validate the name to allow for initialization
//        validate(teamId);

//        Validate.isTrue(teamId.equals(sandbox.getTeamId()));
//        if (!hapiDatabaseService.verifyUser(request, sandbox.getTeamId())) {
//            throw new UnauthorizedUserException("User not authorized to create/update sandbox " + sandbox.getTeamId());
//        }
//        if (dataSet == null) {
//            dataSet = DataSet.NONE;
//        }
//        return hapiDatabaseService.save(sandbox, dataSet);
        return null;
    }

//    @RequestMapping(path = "/clone", method = RequestMethod.PUT)
//    public Sandbox clone(HttpServletRequest request, @NotNull @RequestBody HashMap<String, Sandbox> sandboxes) {
//        Sandbox newSandbox = sandboxes.get("newSandbox");
//        Sandbox clonedSandbox = sandboxes.get("clonedSandbox");
//        Validate.notNull(newSandbox);
//        Validate.notNull(newSandbox.getTeamId());
//        Validate.notNull(clonedSandbox);
//        Validate.notNull(clonedSandbox.getTeamId());
//        if (!hapiDatabaseService.verifyUser(request, clonedSandbox.getTeamId())) {
//            throw new UnauthorizedUserException("User not authorized to clone sandbox " + clonedSandbox.getTeamId());
//        }
//        hapiDatabaseService.clone(newSandbox, clonedSandbox);
//        return newSandbox;
//    }

//    @RequestMapping(method = RequestMethod.GET)
//    public Sandbox get(HttpServletRequest request, @PathVariable String teamId) {
//        Sandbox existing = hapiDatabaseService.get(teamId);
//        if (existing == null) {
//            throw new ResourceNotFoundException("Sandbox [" + teamId + "] is not found");
//        }
//        return existing;
//    }

//    @RequestMapping(method = RequestMethod.DELETE)
//    public boolean delete(HttpServletRequest request, @PathVariable String teamId) {
//        validate(teamId);
//        if (!hapiDatabaseService.verifyUser(request, teamId)) {
//            throw new UnauthorizedUserException("User not authorized to delete sandbox " + teamId);
//        }
//        return hapiDatabaseService.remove(teamId);
//    }

    private void validate(String teamId) {
//        try {
//            SandboxReservedName.valueOf(teamId);
//             oh no, this is a reserved name
//            throw new ForbiddenOperationException("Sandbox [" + teamId + "] is not allowed.");
//            throw new RuntimeException("Sandbox [" + teamId + "] is not allowed.");
//        } catch (IllegalArgumentException e) {
//             good, not a reserved name
//        }
    }


}
